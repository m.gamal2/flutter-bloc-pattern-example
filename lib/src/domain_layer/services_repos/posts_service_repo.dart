import 'package:bloc_pattern/src/data_layer/models/post_model.dart';

abstract class PostsServicesRepo {
  Future<List<PostModel>> getPosts();
}
