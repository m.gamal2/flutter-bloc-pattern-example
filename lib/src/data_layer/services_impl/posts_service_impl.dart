import 'package:bloc_pattern/src/data_layer/models/post_model.dart';
import 'package:bloc_pattern/src/domain_layer/services_repos/posts_service_repo.dart';
import 'package:dio/dio.dart';

class PostsServicesImpl implements PostsServicesRepo {
  final Dio _dio;

  PostsServicesImpl({required Dio dio}) : _dio = dio;

  @override
  Future<List<PostModel>> getPosts() async {
    try {
      final response =
          await _dio.get('https://jsonplaceholder.typicode.com/posts');
      if (response.statusCode == 200) {
        print('=====> ${response.data} <=====');
        final List<dynamic> data = response.data;
        final List<PostModel> posts =
            data.map((e) => PostModel.fromJson(e)).toList();
        return posts;
      } else {
        throw 'Error status code => ${response.statusCode}';
      }
    } on DioException {
      rethrow;
    } catch (e) {
      rethrow;
    }
  }
}
