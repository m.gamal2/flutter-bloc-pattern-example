import 'package:bloc_pattern/src/application_layer/categories_page/page.dart';
import 'package:bloc_pattern/src/application_layer/home_page/page.dart';
import 'package:flutter/material.dart';

class LayoutPage extends StatefulWidget {
  const LayoutPage({Key? key}) : super(key: key);

  @override
  State<LayoutPage> createState() => _LayoutPageState();
}

class _LayoutPageState extends State<LayoutPage> {
  int currentPage = 0;

  final List<Widget> pages = [
    const HomePage(
      key: PageStorageKey('Page1'),
    ),
    const CategoriesPage(
      key: PageStorageKey('Page2'),
    ),
  ];

  final PageStorageBucket bucket = PageStorageBucket();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: PageStorage(
        bucket: bucket,
        child: pages[currentPage],
      ),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: currentPage,
        onTap: (page) => setState(() {
          currentPage = page;
        }),
        items: const [
          BottomNavigationBarItem(icon: Icon(Icons.home), label: ''),
          BottomNavigationBarItem(icon: Icon(Icons.person), label: ''),
        ],
      ),
    );
  }
}

// import 'package:bloc_pattern/src/application_layer/home_page/page.dart';
// import 'package:flutter/material.dart';
//
// class LayoutPage extends StatefulWidget {
//   const LayoutPage({Key? key}) : super(key: key);
//
//   @override
//   State<LayoutPage> createState() => _LayoutPageState();
// }
//
// class _LayoutPageState extends State<LayoutPage> {
//   int selectedPage = 0;
//   final List<Widget> _pages = [
//     Container(
//       color: Colors.orange,
//     ),
//     const HomePage()
//   ];
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       body: Stack(
//         children: _pages
//             .asMap()
//             .map((index, page) => MapEntry(
//             index,
//             Offstage(
//               offstage: selectedPage != index,
//               child: page,
//             )))
//             .values
//             .toList(),
//       ),
//       bottomNavigationBar: BottomNavigationBar(
//         currentIndex: selectedPage,
//         onTap: (index) => setState(() {
//           selectedPage = index;
//         }),
//         items: const [
//           BottomNavigationBarItem(icon: Icon(Icons.home), label: ''),
//           BottomNavigationBarItem(icon: Icon(Icons.person), label: ''),
//         ],
//       ),
//     );
//   }
// }
