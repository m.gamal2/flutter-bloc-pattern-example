import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'cubit/categories_cubit.dart';
import 'widgets/empty_widget.dart';
import 'widgets/error_widget.dart';
import 'widgets/loading_widget.dart';

class CategoriesPage extends StatelessWidget {
  const CategoriesPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Home'),
        centerTitle: true,
      ),
      body: BlocBuilder<CategoriesCubit, CategoriesState>(
        builder: (context, state) {
          final CategoriesCubit homeCubit = CategoriesCubit.get(context);
          if (state is CategoriesInitial) {
            homeCubit.getPosts();
            return const LoadingWidget();
          } else if (state is CategoriesPostsLoadingState) {
            return const LoadingWidget();
          } else if (state is CategoriesPostsLoadedState) {
            return RefreshIndicator(
              onRefresh: () => Future(() => homeCubit.getPosts()),
              child: (state.posts.isNotEmpty)
                  ? ListView.separated(
                      padding: const EdgeInsets.only(top: 10),
                      itemCount: state.posts.length,
                      itemBuilder: (_, index) => Container(
                        height: 80,
                        width: double.infinity,
                        margin: const EdgeInsets.symmetric(horizontal: 15),
                        padding: const EdgeInsets.symmetric(horizontal: 15),
                        decoration: BoxDecoration(
                          color: Colors.purpleAccent.shade100,
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              state.posts[index].title,
                              style: const TextStyle(
                                fontSize: 16,
                                color: Colors.white,
                              ),
                            ),
                            const SizedBox(height: 5),
                            Text(
                              state.posts[index].title,
                              style: TextStyle(
                                fontSize: 12,
                                color: Colors.grey.shade400,
                                overflow: TextOverflow.ellipsis,
                              ),
                            ),
                          ],
                        ),
                      ),
                      separatorBuilder: (_, index) =>
                          const SizedBox(height: 15),
                    )
                  : const EmptyWidget(),
            );
          } else if (state is CategoriesPostsErrorState) {
            return ExceptionWidget(error: state.error);
          } else {
            return const SizedBox();
          }
        },
      ),
    );
  }
}
