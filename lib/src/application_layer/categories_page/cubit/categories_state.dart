part of 'categories_cubit.dart';


abstract class CategoriesState {}

class CategoriesInitial extends CategoriesState {}

class CategoriesPostsLoadingState extends CategoriesState {}

class CategoriesPostsLoadedState extends CategoriesState {
  final List<PostModel> posts;

  CategoriesPostsLoadedState({required this.posts});
}

class CategoriesPostsErrorState extends CategoriesState {
  final String error;

  CategoriesPostsErrorState({required this.error});
}
