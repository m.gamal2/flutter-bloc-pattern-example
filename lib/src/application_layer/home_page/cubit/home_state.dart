part of 'home_cubit.dart';

abstract class HomeState {}

class HomeInitial extends HomeState {}

class HomePostsLoadingState extends HomeState {}

class HomePostsLoadedState extends HomeState {
  final List<PostModel> posts;

  HomePostsLoadedState({required this.posts});
}

class HomePostsErrorState extends HomeState {
  final String error;

  HomePostsErrorState({required this.error});
}
