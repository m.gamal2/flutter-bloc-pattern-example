import 'package:bloc_pattern/src/data_layer/models/post_model.dart';
import 'package:bloc_pattern/src/data_layer/services_impl/posts_service_impl.dart';
import 'package:bloc_pattern/src/domain_layer/services_repos/posts_service_repo.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part 'home_state.dart';

class HomeCubit extends Cubit<HomeState> {
  HomeCubit() : super(HomeInitial());

  static HomeCubit get(BuildContext context) => BlocProvider.of(context);

  final PostsServicesRepo postsServices = PostsServicesImpl(dio: Dio());

  void getPosts() async {
    emit(HomePostsLoadingState());
    try {
      final posts = await postsServices.getPosts();
      emit(HomePostsLoadedState(posts: posts));
    } catch (exception) {
      emit(HomePostsErrorState(error: exception.toString()));
    }
  }
}
