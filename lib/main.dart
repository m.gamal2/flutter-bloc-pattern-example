import 'package:bloc_pattern/src/application_layer/home_page/cubit/home_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'src/application_layer/categories_page/cubit/categories_cubit.dart';
import 'src/application_layer/layout_page/page.dart';

void main() {
  runApp(const BlocApp());
}

class BlocApp extends StatelessWidget {
  const BlocApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(create: (context) => HomeCubit()),
        BlocProvider(create: (context) => CategoriesCubit()),
      ],
      child: MaterialApp(
        home: LayoutPage(),
        // home: HomePage(),
      ),
    );
  }
}
